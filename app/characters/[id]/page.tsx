'use client'

import React from 'react'

import { useCharactersStore } from '../page'
import Link from 'next/link'
type Props = {
  params: {
    id: number
  }
}
export default function Episodes({ params: { id } }: Props) {
  const characters = useCharactersStore((state) => state.characters)
  const char = characters.filter((el) => el.id == id)
  const episodes = char[0]?.episode
  return (
    <div>
      <h2 className="font-black text-2xl my-4">Episodes</h2>
      <ul>
        {episodes?.map((episode, ind) => (
          <li key={ind} className="my-2">
            {episode}
          </li>
        ))}
      </ul>
    </div>
  )
}
