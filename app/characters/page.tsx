'use client'

import React, { useEffect } from 'react'
import { useQuery } from '@tanstack/react-query'
import { create } from 'zustand'
import CharacterCard from '@/components/characterCard'

export const useCharactersStore = create<CharacterState>((set) => ({
  characters: [],
  setCharacters: (characters) => set(() => ({ characters })),
}))

async function getCharactersData(): Promise<{
  info: Info
  results: Character[]
}> {
  try {
    const res = await fetch('https://rickandmortyapi.com/api/character')
    return res.json()
  } catch (err) {
    throw new Error('Failed to fetch data')
  }
}

export default function Characters() {
  const { characters, setCharacters } = useCharactersStore()
  const { data, isLoading, isError } = useQuery({
    queryKey: ['charaters'],
    queryFn: getCharactersData,
  })
  useEffect(() => {
    if (data) {
      setCharacters(data.results)
    }
  }, [data, setCharacters])
  if (isLoading) {
    return <p>Loading...</p>
  }
  if (isError) {
    return <p>Error fetching characters</p>
  }
  return (
    <ul className="flex flex-wrap items-center">
      {characters?.map((char) => (
        <CharacterCard
          key={char.id}
          id={char.id}
          name={char.name}
          status={char.status}
          species={char.species}
          image={char.image}
        ></CharacterCard>
      ))}
    </ul>
  )
}
