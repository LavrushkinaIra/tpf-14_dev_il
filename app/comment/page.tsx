'use client'

import { SubmitHandler, useForm } from 'react-hook-form'
import { ZodType, z } from 'zod'
import { zodResolver } from '@hookform/resolvers/zod'

type FormData = {
  firstname: string
  lastname: string
  email: string
  comment: string
}
const schema: ZodType<FormData> = z.object({
  firstname: z.string().min(2).max(30),
  lastname: z.string().min(2).max(30),
  email: z.string().email(),
  comment: z.string().min(10).max(100),
})

export default function Comment() {
  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
    setValue,
    reset,
  } = useForm<FormData>({
    defaultValues: {
      firstname: '',
      lastname: '',
      email: '',
      comment: '',
    },
    mode: 'onBlur',
    resolver: zodResolver(schema),
  })
  const onSubmit: SubmitHandler<FormData> = (data) => {
    console.log(data)
    reset()
  }
  return (
    <form
      className="flex flex-col items-center"
      onSubmit={handleSubmit(onSubmit)}
    >
      <h2 className="font-black text-2xl my-4">Add New Comment</h2>
      <label className=" flex flex-col font-black text-2x my-4 w-[300px]">
        Firstname
        <input
          className="border border-black rounded-sm  min-h-[40px] w-[300px]"
          {...register('firstname')}
        />
        <div>
          <p className="text-red-600">{errors.firstname?.message}</p>
        </div>
      </label>
      <label className=" flex flex-col font-black text-2x min-h-[40px] my-4 w-[300px]">
        Last name
        <input
          className="border border-black rounded-sm min-h-[40px] w-[300px]"
          {...register('lastname')}
        />
        <div>
          <p className="text-red-600">{errors.lastname?.message}</p>
        </div>
      </label>
      <label className=" flex flex-col font-black text-2x w-[300px]">
        Email
        <input
          className="border border-black rounded-sm min-h-[40px] w-[300px]"
          {...register('email')}
        />
        <div>
          <p className="text-red-600">{errors.email?.message}</p>
        </div>
      </label>
      <label className=" flex flex-col font-black text-2x w-[300px] my-4">
        Comment
        <input
          className="border border-black rounded-sm min-h-[40px] w-[300px]"
          {...register('comment')}
        />
        <div className="err">
          <p className="text-red-600">{errors.comment?.message}</p>
        </div>
      </label>
      <input
        className="bg-blue-700 w-[150px] h-[50px] text-white text-xl rounded-sm"
        type="submit"
      />
    </form>
  )
}
