import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

export default function characterCard({
  id,
  name,
  status,
  species,
  image,
}: CharacterProps) {
  return (
    <li className="border bg-primary rounded p-4 w-80 my-4 mx-4 h-[380px]">
      <div className="relative h-[280px] w-full ">
        <Image
          src={image}
          alt={'Character image'}
          fill
          sizes="300px"
          className="object-cover"
        />
      </div>
      <div className="flex flex-col">
        <Link
          href={`/characters/${id}`}
          className="text-blue-900 font-medium hover:underline"
        >
          {name}
        </Link>
        <span>{status}</span>
        <span>{species}</span>
      </div>
    </li>
  )
}
