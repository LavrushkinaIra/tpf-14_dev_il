import React from 'react'
import Link from 'next/link'

export default function Header() {
  return (
    <div className="bg-full bg-primary border-b h-[80px] flex items-center px-10 gap-4">
      <Link href="/" className="font-black text-2x">
        Rick and Morty
      </Link>
      <ul className="flex items-center">
        <li>
          <Link href="/characters" className="text-blue-500 mx-4">
            Персонажі
          </Link>
        </li>
        <li>
          <Link href="/comment" className="text-blue-500 mx-4">
            Коментар
          </Link>
        </li>
      </ul>
    </div>
  )
}
