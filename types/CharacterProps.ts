type CharacterProps = {
  id: number
  name: string
  status: string
  species: string
  image: string
}
